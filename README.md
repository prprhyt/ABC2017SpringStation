# ABC2017SpringStation
これは日本Andoridの会学生部が制作するアプリケーションのリポジトリです。
基本的に外部の方からのマージリクエストにはお答えできません。

# ビルド注意点
app/res/values内に、google_maps_api.xmlを配置する必要があります。
セキュリティの問題上、gitignoreでgit管理外に指定しています。
上記ファイルについては @takanakahiko が管理しているので、問い合わせてください。